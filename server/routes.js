const httpProxy = require('express-http-proxy')

exports.init = (app) => {

  const pessoasApiProxy = httpProxy(`http://localhost:${process.env.API_PESSOAS_PORT}/pessoas`)
  const dadosGeograficosApiProxy = httpProxy(`http://localhost:${process.env.API_DADOS_GEOGRAFICOS_PORT}/dados-geograficos`)

  app.use('/pessoas', (req, res, next) => {
    pessoasApiProxy(req, res, next)
  });
  app.use('/dados-geograficos', (req, res, next) => {
    dadosGeograficosApiProxy(req, res, next)
  });
}

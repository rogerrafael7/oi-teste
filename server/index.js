const path = require('path')
require('dotenv').config({
  path: path.resolve(__dirname, '../.env.development')
})
const redis = require('redis')
const { RateLimiterRedis } = require('rate-limiter-flexible')

const redisClient = redis.createClient(6379, "localhost")
redisClient.on("error", function (err) {
  console.log("Redis error encountered", err);
});

redisClient.on("end", function() {
  console.log("Redis connection closed");
});

const rateLimiter = new RateLimiterRedis({
  storeClient: redisClient,
  keyPrefix: 'middleware',
  points: 10, // 10 requests
  duration: 1, // per 1 second by IP
});

const rateLimiterMiddleware = (req, res, next) => {
  rateLimiter.consume(req.ip)
    .then(() => {
      next();
    })
    .catch(() => {
      res.status(429).send('Bloqueio por muitas requisições');
    });
};

const http = require('http')
const express = require('express')
const cors = require('cors')
const routes = require('./routes')
const PORT = process.env.MAIN_SERVER_PORT

const app = express()
const server = http.createServer(app)

app.use(rateLimiterMiddleware)
app.use(cors())

routes.init(app)

server.listen(PORT, () => {
  console.log(`Servidor Principal rodando na porta: ${PORT}`)
});

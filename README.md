### Mapeamento

- Diretório: **server** - É o servidor principal responsável por direcionar as requisições para os serviços específicos. 
É por meio dele que todas requisições, de um frontend, trafegam.
- Diretório: **base-api** - Módulo utilitário que já propõe uma estrutura, padronização e reutilização na criação de uma API simples
- Diretório: **web-client** - Página web do projeto frontend
- Diretório: **api-pessoas** - Expõe os endpoints responsável por tratar os dados de pessoas. Usa o **base-api** em sua criação  
- Diretório: **api-dados-geograficos** - Expõe os endpoints responsável por tratar os dados geográficos, tais como: UF e Município. Usa o **base-api** em sua criação  

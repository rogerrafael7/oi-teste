import request from './helper/request'

const service = {
  getPessoas: async (params = {}) => {
    return request.get('/pessoas', params)
  },
  getPessoa: async (id) => {
    return request.get(`/pessoas/${id}`)
  },
  inserirPessoa(values) {
    return request.post('/pessoas', values)
  },
  atualizarPessoa (id, values) {
    return request.put(`/pessoas/${id}`, values)
  },
  removerPessoa (id) {
    return request.delete(`/pessoas/${id}`)
  },

  getUfs: async () => {
    return request.get(`/dados-geograficos/uf`)
  },
  getCidadesMunicipiosPaginado (params) {
    return request.get('/dados-geograficos/municipios', params)
  },
  getCidadesMunicipios: async (uf = null) => {
    return (await request.get(`/dados-geograficos/uf/${uf}/municipios`)).rows
  },
  inserirCidade(values) {
    return request.post('/dados-geograficos/municipios', values)
  },
  atualizarCidade (id, values) {
    return request.put(`/dados-geograficos/municipios/${id}`, values)
  },
  removerCidade (id) {
    return request.delete(`/dados-geograficos/municipios/${id}`)
  }
}

export default service

import axios from 'axios'

const hostUrl = process.env.REACT_APP_HOST.replace(/\/$/, '')
const normalizePathUrl = (pathUrl) => `/${pathUrl.replace(/^\//, '')}`

console.log(hostUrl)

export default {
  async get (pathUrl, params) {
    return (
      await axios.get(`${hostUrl}${normalizePathUrl(pathUrl)}`, { params })
    ).data
  },
  async post (pathUrl, params) {
    return (
      await axios.post(`${hostUrl}${normalizePathUrl(pathUrl)}`, params)
    ).data
  },
  async put (pathUrl, params) {
    return (
      await axios.put(`${hostUrl}${normalizePathUrl(pathUrl)}`, params)
    ).data
  },
  async delete (pathUrl, params) {
    return (
      await axios.delete(`${hostUrl}${normalizePathUrl(pathUrl)}`, { params })
    ).data
  }
}

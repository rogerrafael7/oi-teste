import lodash from 'lodash'

const notifierHelper = {
  sendError (error, defaultMessage = 'Erro durante o processamento') {
    let message = defaultMessage
    console.error(error, message)
    if (['CUSTOM_EXCEPTION', 'DEFAULT_EXCEPTION'].includes(lodash.get(error, 'response.data.info.type'))) {
      message = error.response.data.info.message
    }
    alert(message)
  }
}

export default notifierHelper

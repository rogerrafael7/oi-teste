import React, { Fragment, useCallback, useEffect, useState } from 'react'
import GridData from '../components/GridData'
import service from '../service'
import { Button } from '@material-ui/core'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import IconButton from '@material-ui/core/IconButton'
import { ArrowBack, Close } from '@material-ui/icons'
import { Link } from 'react-router-dom'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import TextField from '@material-ui/core/TextField'
import Snackbar from '@material-ui/core/Snackbar'
import notifierHelper from '../helper/notifier'

const CidadesPage = () => {
  const [cidades, setCidades] = useState([])
  const [countPages, setCountPages] = useState(0)
  const [currentPage, setCurrentPage] = useState(1)
  const [openForm, setOpenForm] = useState(false)
  const [lastPage, setLastPage] = useState(false)
  const [openAlert, setOpenAlert] = useState(false)
  const [messageAlert, setMessageAlert] = useState(null)
  const [ufs, setUfs] = useState([])
  const [siglaUf, setSiglaUf] = useState(null)
  const [nomeMunicipio, setNomeMunicipio] = useState(null)
  const [idCidadeMunicipio, setIdCidadeMunicipio] = useState(null)

  const memoizedUfs = (async () => {
    setUfs(
      await service.getUfs()
    )
  })

  const memoizedCidades = useCallback(async () => {
    const size = 5
    const cidades = await service.getCidadesMunicipiosPaginado({
      page: currentPage,
      size
    })
    cidades.rows = cidades.rows.map((row) => ({
      ...row,
      acoes: (<Fragment>
        <Button onClick={() => {
          setIdCidadeMunicipio(row._id)
          setSiglaUf(row.siglaUf)
          setNomeMunicipio(row.nomeMunicipio)
          setOpenForm(true)
        }}>Editar</Button>
        <Button onClick={async () => {
          try {
            await service.removerCidade(row._id)
            await memoizedCidades()
          } catch (error) {
            notifierHelper.sendError(error, 'Não foi possível remover a cidade')
          }
        }}>Remover</Button>
      </Fragment>)
    }))
    setCidades(cidades.rows)
    setCountPages(cidades.count / size)
    setLastPage(cidades.size < size)
  }, [currentPage])

  const salvar = async () => {
    let message = 'Existem erros no preenchimento do formulário'
    try {
      setOpenAlert(false)
      setMessageAlert(null)
      if (!nomeMunicipio){
        message = 'Nome da cidade é obrigatório'
        throw new Error('Erro de validação')
      }
      const values = {
        siglaUf,
        nomeMunicipio
      }
      if (idCidadeMunicipio) {
        await service.atualizarCidade(idCidadeMunicipio, values)
      } else {
        await service.inserirCidade(values)
      }
      memoizedCidades()
      setOpenAlert(true)
      setMessageAlert('Cidade salva com sucesso')
      setOpenForm(false)
    } catch (error) {
      notifierHelper.sendError(error, message)
    }
  }

  useEffect(() => {
    setOpenAlert(false)
    if (!openForm) {
      setSiglaUf(null)
      setNomeMunicipio(null)
      setIdCidadeMunicipio(null)
    }
  }, [openForm])

  useEffect(() => {
    memoizedCidades()
    memoizedUfs()
  }, [])

  return (
    <FormControl className={'CidadesPage'}>
      <div>
        <Link to={'/'}>
          <IconButton onClick={() => {
            setOpenForm(true)
          }}
          >
            <ArrowBack/>
          </IconButton>
        </Link>
        <Button color={'primary'} variant={'outlined'}
                onClick={() => {
                  setOpenForm(true)
                }}
        >Adicionar Novo</Button>
        <br/>
      </div>
      <GridData
        headers={[
          { name: 'siglaUf', alias: 'UF' },
          { name: 'nomeMunicipio', alias: 'Cidade' },
          { name: 'acoes', alias: 'Ações' }
        ]}
        rows={cidades}
      />

      <Dialog open={openForm}
              onBackdropClick={() => setOpenForm(false)}
      >
        <DialogTitle id="form-dialog-title">
          Manter Cidade
        </DialogTitle>
        <DialogContent>
          <div style={{padding: 15}}>
            <FormControl style={{ minWidth: 200 }}>
              <InputLabel>UF</InputLabel>
              <Select value={siglaUf}
                      onChange={({ target: { value } }) => setSiglaUf(value)}
                      disabled={!!idCidadeMunicipio}
              >
                {ufs.map(({ sigla, nomeUf }, index) => (
                  <MenuItem key={index} value={sigla}>{nomeUf}</MenuItem>
                ))}
              </Select>
              <br/>
              <TextField label={'Nome da cidade'}
                         value={nomeMunicipio}
                         onChange={({ target: { value } }) => setNomeMunicipio(value)}
                         required
              />
            </FormControl>
            <Snackbar open={openAlert}
                      anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                      }}
                      message={messageAlert}
                      action={
                        <React.Fragment>
                          <IconButton size="small" aria-label="close" color="inherit" onClick={() => setOpenAlert(false)}>
                            <Close fontSize="small" />
                          </IconButton>
                        </React.Fragment>
                      }
            >
            </Snackbar>
          </div>
          <br/>
          <Button onClick={salvar}>Salvar</Button>
        </DialogContent>
      </Dialog>

    </FormControl>
  )
}

export default CidadesPage

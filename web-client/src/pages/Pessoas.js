import React, { Fragment, useCallback, useEffect, useState } from 'react'
import GridData from '../components/GridData'
import service from '../service'
import { Button } from '@material-ui/core'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import IconButton from '@material-ui/core/IconButton'
import { ArrowBack, Close } from '@material-ui/icons'
import { Link } from 'react-router-dom'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import TextField from '@material-ui/core/TextField'
import Snackbar from '@material-ui/core/Snackbar'
import InputMask from 'react-input-mask'
import InputLabel from '@material-ui/core/InputLabel'
import notifierHelper from '../helper/notifier'

const PessoasPage = () => {
  const [cidades, setCidades] = useState([])
  const [ufs, setUfs] = useState([])
  const [pessoas, setPessoas] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [openForm, setOpenForm] = useState(false)
  const [openAlert, setOpenAlert] = useState(false)
  const [messageAlert, setMessageAlert] = useState()

  const [idPessoa, setIdPessoa] = useState()
  const [tipoPessoa, setTipoPessoa] = useState()
  const [cpf, setCpf] = useState()
  const [cnpj, setCnpj] = useState()
  const [uf, setUf] = useState()
  const [cidade, setCidade] = useState()
  const [cep, setCep] = useState()
  const [telefone, setTelefone] = useState()
  const [nomeCompleto, setNomeCompleto] = useState()
  const [razaoSocial, setRazaoSocial] = useState()
  const [dataNascimento, setDataNascimento] = useState()

  const memoizedUfs = (async () => {
    setUfs(
      await service.getUfs()
    )
  })
  const memoizedPessoas = (async () => {
    const pessoas = (await service.getPessoas()).rows
      .map((row) => ({
      ...row,
      acoes: (<Fragment>
        <Button onClick={() => {
          setIdPessoa(row._id)
          setTipoPessoa(row.tipoPessoa)
          if (row.tipoPessoa === 'F') {
            setCpf(row.cpf)
            setNomeCompleto(row.nomeCompleto)
            setDataNascimento(row.dataNascimento)
          } else if (row.tipoPessoa === 'J') {
            setCnpj(row.cnpj)
            setRazaoSocial(row.razaoSocial)
          }
          setUf(row.uf)
          setCidade(row.cidade)
          setCep(row.cep)
          setTelefone(row.telefone)
          setOpenForm(true)
        }}>Editar</Button>
        <Button onClick={async () => {
          try {
            await service.removerPessoa(row._id)
            await memoizedPessoas()
          } catch (error) {
            notifierHelper.sendError(error, 'Não foi possível remover a pessoa')
          }
        }}>Remover</Button>
      </Fragment>)
    }))

    setPessoas(pessoas)
  })

  const memoizedCidades = useCallback(async () => {
    setCidades(
      await service.getCidadesMunicipios(uf)
    )
  }, [uf])

  const salvar = async () => {
    let message = 'Existem erros no preenchimento do formulário'
    try {
      setOpenAlert(false)
      setMessageAlert(null)

      const values = {
        tipoPessoa,
        uf,
        cidade,
        cep,
        telefone
      }

      if (!tipoPessoa) {
        message = `Campo: tipoPessoa é obrigatório`
        throw new Error('Erro de validação')
      }
      if (tipoPessoa === 'F') {
        if (!cpf) {
          message = `Campo: cpf é obrigatório`
          throw new Error('Erro de validação')
        }
        if (!nomeCompleto) {
          message = `Campo: nomeCompleto é obrigatório`
          throw new Error('Erro de validação')
        }
        if (!dataNascimento) {
          message = `Campo: dataNascimento é obrigatório`
          throw new Error('Erro de validação')
        }
        values.cpf = cpf
        values.nomeCompleto = nomeCompleto
        values.dataNascimento = dataNascimento
      } else if (tipoPessoa === 'J') {
        if (!cnpj) {
          message = `Campo: cnpj é obrigatório`
          throw new Error('Erro de validação')
        }
        if (!razaoSocial) {
          message = `Campo: razaoSocial é obrigatório`
          throw new Error('Erro de validação')
        }
        values.cnpj = cnpj
        values.razaoSocial = razaoSocial
      }
      if (!uf) {
        message = `Campo: uf é obrigatório`
        throw new Error('Erro de validação')
      }
      if (!cidade) {
        message = `Campo: cidade é obrigatório`
        throw new Error('Erro de validação')
      }
      if (!cep) {
        message = `Campo: cep é obrigatório`
        throw new Error('Erro de validação')
      }
      if (!telefone) {
        message = `Campo: telefone é obrigatório`
        throw new Error('Erro de validação')
      }

      if (idPessoa) {
        await service.atualizarPessoa(idPessoa, values)
      } else {
        await service.inserirPessoa(values)
      }
      memoizedPessoas()
      setOpenAlert(true)
      setMessageAlert('Pessoa salva com sucesso')
      setOpenForm(false)
    } catch (error) {
      notifierHelper.sendError(error, message)
    }
  }

  useEffect(() => {
    setOpenAlert(false)
    if (!openForm) {
      setTipoPessoa()
      setCpf()
      setCnpj()
      setUf()
      setCidade()
      setCep()
      setTelefone()
      setNomeCompleto()
      setRazaoSocial()
      setDataNascimento()
      setIdPessoa()
    }
  }, [openForm])

  useEffect(() => {
    memoizedUfs()
    memoizedPessoas()
  }, [])
  useEffect(() => {
    console.log(uf)
    memoizedCidades()
  }, [uf])

  return (
    <div className={'PessoasPage'}>
      <div>
        <Link to={'/'}>
          <IconButton onClick={() => {
            setOpenForm(true)
          }}
          >
            <ArrowBack/>
          </IconButton>
        </Link>
        <Button color={'primary'} variant={'outlined'}
                onClick={() => {
                  setOpenForm(true)
                }}
        >Adicionar Novo</Button>
        <br/>
      </div>
      <GridData
        headers={[
          { name: 'tipoPessoa', alias: 'Tipo de Pessoa' },
          { name: 'cpf', alias: 'CPF' },
          { name: 'cnpj', alias: 'CNPJ' },
          { name: 'uf', alias: 'UF' },
          { name: 'cidade', alias: 'Cidade' },
          { name: 'cep', alias: 'CEP' },
          { name: 'telefone', alias: 'Telefone' },
          { name: 'nomeCompleto', alias: 'Nome Completo' },
          { name: 'razaoSocial', alias: 'Razão Social' },
          { name: 'acoes', alias: 'Ações' }
        ]}
        rows={pessoas}
      />

      <Dialog open={openForm}
              onBackdropClick={() => setOpenForm(false)}
      >
        <DialogTitle id="form-dialog-title">
          Manter Pessoa
        </DialogTitle>
        <DialogContent style={{ width: 500 }}>
          <div style={{ padding: 15 }}>
            <FormControl fullWidth>
              <InputLabel>Tipo de Pessoa</InputLabel>
              <Select value={tipoPessoa}
                      onChange={({ target: { value } }) => setTipoPessoa(value)}
                      fullWidth
              >
                <MenuItem value={'F'}>Pessoa Física</MenuItem>
                <MenuItem value={'J'}>Pessoa Jurídica</MenuItem>
              </Select>
            </FormControl>
            {
              tipoPessoa === 'F' ?
                (
                  <div>
                    <TextField
                      fullWidth
                      label={'Nome Completo'}
                      value={nomeCompleto}
                      onChange={({ target: { value } }) => setNomeCompleto(value)}
                    />
                    <TextField
                      fullWidth
                      type={'date'}
                      label={'Data de Nascimento'}
                      value={dataNascimento}
                      onChange={({ target: { value } }) => setDataNascimento(value)}
                    />
                    <InputMask
                      mask="999.999.999-99"
                      disabled={false}
                      value={cpf}
                      onChange={({ target: { value } }) => setCpf(value)}
                    >
                      {() => <TextField label="Informe seu CPF" fullWidth/>}
                    </InputMask>
                  </div>
                )
                : tipoPessoa === 'J' &&
                (
                  <div>
                    <TextField
                      fullWidth
                      label={'Razão Social'}
                      value={razaoSocial}
                      onChange={({ target: { value } }) => setRazaoSocial(value)}
                    />
                    <InputMask
                      fullWidth
                      mask="99.999.999/9999-99"
                      disabled={false}
                      value={cnpj}
                      onChange={({ target: { value } }) => setCnpj(value)}
                    >
                      {() => <TextField label="Informe seu CNPJ" fullWidth/>}
                    </InputMask>
                  </div>
                )
            }

            <div>
              <InputMask
                mask="99.999-999"
                disabled={false}
                value={cep}
                onChange={({ target: { value } }) => setCep(value)}
              >
                {() => <TextField fullWidth label={'CEP'}/>}
              </InputMask>
            </div>

            <FormControl fullWidth>
              <InputLabel>UF</InputLabel>
              <Select
                fullWidth
                value={uf}
                onChange={({ target: { value } }) => setUf(value)}
              >
                {ufs.map(({ sigla, nomeUf }, index) => (
                  <MenuItem key={index} value={sigla}>{nomeUf}</MenuItem>
                ))}
              </Select>
            </FormControl>

            <FormControl fullWidth>
              <InputLabel>Cidade</InputLabel>
              <Select
                fullWidth
                value={cidade}
                onChange={({ target: { value } }) => setCidade(value)}
              >
                {cidades.map(({ nomeMunicipio }, index) => (
                  <MenuItem key={index} value={nomeMunicipio}>{nomeMunicipio}</MenuItem>
                ))}
              </Select>
            </FormControl>

            <div>
              <TextField
                fullWidth
                label={'Telefone'}
                value={telefone}
                onChange={({ target: { value } }) => setTelefone(value)}
              />
            </div>

            <Snackbar open={openAlert}
                      anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left'
                      }}
                      message={messageAlert}
                      action={
                        <React.Fragment>
                          <IconButton size="small" aria-label="close" color="inherit"
                                      onClick={() => setOpenAlert(false)}>
                            <Close fontSize="small"/>
                          </IconButton>
                        </React.Fragment>
                      }
            >
            </Snackbar>
          </div>
          <br/>
          <Button onClick={salvar}>Salvar</Button>
        </DialogContent>
      </Dialog>

    </div>
  )
}

export default PessoasPage

import React, { Fragment, useCallback, useEffect, useState } from 'react'
import RadioGroup from '@material-ui/core/RadioGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormControl from '@material-ui/core/FormControl'
import Radio from '@material-ui/core/Radio'
import { Button, Grid } from '@material-ui/core'
import TextField from '@material-ui/core/TextField'
import InputMask from 'react-input-mask'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import service from '../service'
import InputLabel from '@material-ui/core/InputLabel'
import { Link } from 'react-router-dom'

const style = {
  boxResult: {
    texAlign: 'justify',
    padding: 10,
    border: '3px solid #eee'
  },
  areaCentral: {
    marginTop: 20,
    width: '100%'
  },
  areaDeBusca: {
    marginTop: 20,
    width: '100%',
    margin: '15px 0'
  },
  footer: {
    display: 'flex',
    flexDirection: 'column',
    width: 'auto',
    maxWidth: 200
  }
}

const HomePage = () => {
  const [tipoPessoa, setTipoPessoa] = useState('F')
  const [cpf, setCpf] = useState(null)
  const [cnpj, setCnpj] = useState(null)
  const [uf, setUf] = useState(null)
  const [cidade, setCidade] = useState(null)
  const [buscaExecutada, setBuscaExecutada] = useState(false)
  const [disabilitarBtnBuscar, setDisabilitarBtnBuscar] = useState(true)
  const [ufs, setUfs] = useState([])
  const [cidades, setCidades] = useState([])
  const [pessoaEncontradas, setPessoaEncontradas] = useState([])

  const memoizedUfs = useCallback(async () => {
    setUfs(
      await service.getUfs()
    )
  }, [ufs])

  const memoizedCidades = useCallback(async () => {
    setCidades(
      await service.getCidadesMunicipios(uf)
    )
  }, [uf])

  const memoizedPessoasEncontradas = useCallback(async () => {
    const query = {
      $or: [
        {
          cpf: cpf || null,
          cnpj: cnpj || null,
          uf: uf || null,
          cidade: cidade || null
        }
      ]
    }
    if (cpf || cnpj || uf || cidade) {
      query.$or.push({
        ...(cpf ? { cpf } : {}),
        ...(cnpj ? { cnpj } : {}),
        ...(uf ? { uf } : {}),
        ...(cidade ? { cidade } : {})
      })
    }
    setPessoaEncontradas(
      (await service.getPessoas({
        query
      })).rows
    )
    setBuscaExecutada(true)
  }, [cpf, cnpj, uf, cidade])

  useEffect(() => {
    if ((cpf || cnpj) && uf && cidade) {
      setDisabilitarBtnBuscar(false)
    } else {
      setDisabilitarBtnBuscar(true)
    }
  }, [cpf, cnpj, uf, cidade])

  useEffect(() => {
    memoizedUfs()
  }, [])

  useEffect(() => {
    setCidade(null)
    memoizedCidades()
  }, [uf])

  useEffect(() => {
    setCnpj(null)
    setCpf(null)
  }, [tipoPessoa])

  return (
    <div className="HomePage" style={{ width: '100%' }}>
      <FormControl component="fieldset">
        <RadioGroup value={tipoPessoa} onChange={({ target: { value } }) => setTipoPessoa(value)}>
          <FormControlLabel value="F" control={<Radio/>} label="Pessoa Física"/>
          <FormControlLabel value="J" control={<Radio/>} label="Pessoa Jurídica"/>
        </RadioGroup>
      </FormControl>

      <Grid container>
        <Grid item md={9}>
          {
            tipoPessoa === 'F' ? (
              <Grid container>
                <Grid item md={4}>
                  <InputMask
                    mask="999.999.999-99"
                    disabled={false}
                    onChange={({ target: { value } }) => setCpf(value)}
                  >
                    {() => <TextField label="Informe seu CPF" fullWidth/>}
                  </InputMask>
                </Grid>
              </Grid>
            ) : tipoPessoa === 'J' && (
              <Grid container>
                <Grid item md={4}>
                  <InputMask
                    mask="99.999.999/9999-99"
                    disabled={false}
                    onChange={({ target: { value } }) => setCnpj(value)}
                  >
                    {() => <TextField label="Informe seu CNPJ" fullWidth/>}
                  </InputMask>
                </Grid>
              </Grid>
            )
          }
          <Grid container style={style.areaCentral}>
            <Grid item md={6}>
              <FormControl style={{ minWidth: 200 }}>
                <InputLabel htmlFor="ufs">UF</InputLabel>
                <Select
                  onChange={({ target: { value } }) => setUf(value)}
                  inputProps={{
                    id: 'ufs'
                  }}
                  fullWidth
                >
                  {
                    ufs.map(({ sigla, nomeUf }, i) => (
                      <MenuItem key={i} value={sigla}>{nomeUf}</MenuItem>
                    ))
                  }
                </Select>
              </FormControl>
            </Grid>
            <Grid item md={6}>
              <FormControl style={{ minWidth: 200 }}>
                <InputLabel htmlFor="cidades">Cidade</InputLabel>
                <Select
                  onChange={({ target: { value } }) => setCidade(value)}
                  inputProps={{
                    id: 'cidades'
                  }}
                  fullWidth
                >
                  {
                    cidades.map(({ nomeMunicipio }, i) => (
                      <MenuItem key={i} value={nomeMunicipio}>{nomeMunicipio}</MenuItem>
                    ))
                  }
                </Select>
              </FormControl>
            </Grid>
          </Grid>

        </Grid>
        <Grid item md={3}>
          {
            buscaExecutada && (
              <div style={style.boxResult}>
                {
                  pessoaEncontradas && pessoaEncontradas.length
                    ?
                    (
                      <div style={{ fontSize: '0.7em' }}>
                        <b>Pessoas Encontradas</b> <br/>
                        {
                          pessoaEncontradas.map((pessoaEncontrada, i) => (
                            <div key={i} style={{ borderBottom: '1px solid #ddd', paddingBottom: 5 }}>
                              {
                                pessoaEncontrada.tipoPessoa === 'F'
                                  ? (
                                    <Fragment>
                                      <b>Nome completo:</b> {pessoaEncontrada.nomeCompleto} <br/>
                                      <b>CPF:</b> {pessoaEncontrada.cpf} <br/>
                                    </Fragment>
                                  )
                                  : (
                                    <Fragment>
                                      <b>Razão Social:</b> {pessoaEncontrada.razaoSocial} <br/>
                                      <b>CNPJ:</b> {pessoaEncontrada.cnpj} <br/>
                                    </Fragment>
                                  )
                              }
                              <b>Telefone:</b> {pessoaEncontrada.telefone} <br/>
                              <b>UF:</b> {pessoaEncontrada.uf} <br/>
                              <b>Cidade:</b> {pessoaEncontrada.cidade} <br/>
                            </div>
                          ))
                        }
                      </div>
                    )
                    : (
                      <div>Nenhuma pessoa encontrada</div>
                    )
                }
              </div>
            )
          }
        </Grid>
      </Grid>
      <Grid container style={style.areaDeBusca}>
        <Grid item>
          <Button
            disabled={disabilitarBtnBuscar}
            variant={'outlined'}
            onClick={() => {
              memoizedPessoasEncontradas()
            }}>Buscar</Button>
        </Grid>
      </Grid>

      <div style={style.footer}>
        <Link to="/pessoas">Gerenciar Pessoas</Link>
        <Link to="/cidades">Gerenciar Cidades</Link>
      </div>
    </div>
  )
}

export default HomePage

import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import HomePage from './pages/Home'
import PessoasPage from './pages/Pessoas'
import CidadesPage from './pages/Cidades'

const style = {
  main: {
    display: 'flex',
    flexDirection: 'column',
    alignCenter: 'center',
    justifyContent: 'center',
    width: '95%',
    maxWidth: 800,
    padding: 10,
    border: '1px solid #ddd',
    margin: '10px auto'
  }
}

const App = () => {
  return (
    <div className="App" style={style.main}>
      <Router>
        <Switch>
          <Route path={'/pessoas'}>
            <PessoasPage />
          </Route>
          <Route path={'/cidades'}>
            <CidadesPage />
          </Route>
          <Route path={'/'}>
            <HomePage />
          </Route>
        </Switch>
      </Router>
    </div>
  )
}

export default App

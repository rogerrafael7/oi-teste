import React from 'react'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import Table from '@material-ui/core/Table'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import Paper from '@material-ui/core/Paper'

const GridData = ({ headers = [], rows = [] } = {}) => {
  const keys = headers.map(({ name }) => name)
  const mapHeaders = {}
  for (const header of headers) {
    if (keys.includes(header.name)) {
      mapHeaders[header.name] = header.alias
    }
  }
  return (
    <TableContainer component={Paper} style={{ width: '100%' }}>
      <Table style={{ width: '100%' }}>
        <TableHead>
          <TableRow>
            {keys.map((key, index) => (
              <TableCell key={`th:${key}_${index}`}>{mapHeaders[key]}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row, index) => {
            return (
              <TableRow key={`trow:${index}`}>
                { keys.map((column, j) => (
                  <TableCell key={`td:${column}_${j}`}>
                    {row[column]}
                  </TableCell>
                )) }
              </TableRow>
            )
          })}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default GridData

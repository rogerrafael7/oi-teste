require('./globals/CustomException')

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const responser = require('./middleware/responser')
const factoryControllers = require('./factory-controllers')

module.exports = async (name, controllersDirectoryPath, PORT) => {
  app.use(bodyParser.json())
  app.use(responser)

  await factoryControllers.init(controllersDirectoryPath, app)

  app.listen(PORT, () => {
    console.log(`API: ${name} - rodando na porta: ${PORT}`)
  })
}
